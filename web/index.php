<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . '../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\ValidatorServiceProvider;
use JDesrosiers\Silex\Provider\CorsServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Main\RedTubeRequest;

/*
 * Register application
 */
$app = new Application();

$app['redTubeRequest'] = $app->share(function() {
	return new RedTubeRequest();
});

$app->register(new ValidatorServiceProvider());

$app->register(new CorsServiceProvider(), [
    "cors.allowOrigin" => "*",
]);

$app->get('/', function(Request $request) use($app) {
	return $app->json($app['redTubeRequest']->getAll());
});

$app->get('/{id}', function(Request $request, $id) use($app) {
	return $app->json($app['redTubeRequest']->getById($id));
})->assert('id', '\d+');

$app->after($app["cors"]);

$app->run();