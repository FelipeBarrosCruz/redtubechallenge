<?php
namespace Main;

class RedTubeRequest
{
	private $baseUrl = 'http://api.redtube.com/';

	private function buildParams($params)
	{
		return '?' . http_build_query($params);
	}

	private function request($params)
	{
		$content = json_decode(file_get_contents($this->baseUrl . $this->buildParams($params)), true);

		$content = (!empty($content))
				 ? $content
				 : ['status' => false];

		return $content;
	}

	public function getAll()
	{
		$params = [
			'data' 		=> 'redtube.Videos.searchVideos',
			'output'	=> 'json',
			'search'	=> 'hard',
			'tags[]' 	=> 'Teen',
			'thumbsize' => 'medium'
		];

		return $this->request($params);
	}

	private function getDetails($videoId)
	{
		$params = [
			'data' 		=> 'redtube.Videos.getVideoById',
			'output'	=> 'json',
			'thumbsize' => 'all',
			'video_id'	=> $videoId
		];

		return $this->request($params);
	}

	private function getEmbed($videoId)
	{
		$params = [
			'data' 		=> 'redtube.Videos.getVideoEmbedCode',
			'output'	=> 'json',
			'video_id'	=> $videoId
		];

		return $this->request($params);
	}

	public function getById($videoId)
	{
		$details = $this->getDetails($videoId);
		$embed  = $this->getEmbed($videoId);

		if (!isset($details['video'], $embed['embed']))
			return ['status' => false];

		$details['video']['embed'] = $embed['embed'];

		return $details;
	}
}